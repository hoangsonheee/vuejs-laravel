var vueInstance = new Vue({
    el: '#app',
    data: {
        title: 'Áo arsenal mùa giải 2020-2021 vải xịn xò',
        url: 'https://www.sporter.vn/product/ao-arsenal-san-khach',
        target: '_blank',
        price: 20000,
        sale: 0.1,
        selectedProduct: 2,
        cardNumber: 1,
        listProductDetail: [
            {
                image: './images/ao.jpg',
                quantity:3,
                textColor: 'Áo đôi'
            },
            
            {
                image: './images/1.jpg',
                quantity: 0,
                textColor: 'Màu trắng'
            }, {
                image: './images/2.jpg',
                quantity: 8,
                textColor: 'Màu Đỏ'
            }, {
                image: './images/3.jpg',
                quantity: 2,
                textColor: 'Màu Xanh'
            }, 
        ],
        listDesc: [
            'Chất liệu: polyester và thun',
            'Thoát mồ hôi tốt',
            'Áo thun cổ tròn thể thao Hiye chuyên được may từ chất liệu nilon thoáng mát',
            'Kết hợp thêm sợi thun tạo độ co giãn giúp người tiêu dùng thoải mái khi mặc',
            'Chất liệu: polyester và thun'
        ],
       
  
        
    },
    methods: {
        handleClickColor(e, index) {
            this.selectedProduct = index;
            // console.log(e, index, this);
        },
        classActive(index) {
            return {
                active: this.selectedProduct === index
            }
        },
        handleAddToCart(e) {
            if(this.cardNumber + 1 > this.getProduct.quantity) {
                alert('So luong khong du');
            } else {
                this.cardNumber = this.cardNumber + 1;
            }
            console.log(e.target)
        }

    
    },
    computed: {
        formatOriginalPrice() {
            var number = this.price;
            return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(number);
        },
        formatFinalPrice() {
            var number = this.price - this.sale * this.price;
            return new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'VND' }).format(number);
        },
        getProduct() {
            let index = this.selectedProduct;
            return this.listProductDetail[index];
        }
    }
});
